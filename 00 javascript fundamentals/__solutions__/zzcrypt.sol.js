// [SOLUCIÓN]

var secret = "sq esqct rts moh egf sql lgsxeogftl tl dqlztkstdgfegrtpqcqlekohzyxfrqdtfzqsl";

var plain = "abcdefghijklmnopqrstuvwxyz";
var cipher = "qwertyuiopasdfghjklzxcvbnm";

function decryptLetter(letter) {
  return letter !== " " ? plain[cipher.indexOf(letter)]: " ";
}

function decrypt(secret) {
  return secret.split("").map(decryptLetter).join("");
}

console.log(decrypt(secret));
